import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import LoadingScreen from './src/screens/LoadingScreen';
import LoginScreen from './src/screens/LoginScreen';
import SingupScreen from './src/screens/SingupScreen';
import DashboardScreen from './src/screens/DashboardScreen';

import { ApiService } from './src/api';


ApiService.ApiInit.Init();

export default class App extends React.Component {
  render() {
    return <AppNavigator />;
  }
}

const AppSwitchNavigator = createSwitchNavigator({
  LoadingScreen,
  LoginScreen,
  SingupScreen,
  DashboardScreen
});

const AppNavigator = createAppContainer(AppSwitchNavigator);