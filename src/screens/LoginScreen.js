import React, { Component } from 'react';
import { SafeAreaView, View, Text, StyleSheet, TouchableOpacity, TextInput, ScrollView, KeyboardAvoidingView, Alert } from 'react-native';

import firebase from 'firebase';

class LoginScreen extends Component {
    constructor(props){
        super(props);

        this.state = ({
            email: '',
            password: ''
        });
    }

    _showAlert = (header, body) => {
        Alert.alert(
            header,
            body,
            [
                {text: 'OK', onPress: () => this.props.navigation.goBack()},
            ],
            { cancelable: false }
        );
    }

    loginUser = (email,password) => {
        firebase.auth().signInWithEmailAndPassword(email,password).then(() => {
            this.props.navigation.navigate('DashboardScreen');
        }).catch(error => {
            if(error.code=='auth/user-not-found'){
                this._showAlert('Usuário não cadastrado!', 'O e-mail e senha informados são inválidos, confira as informações.');
            }else if(error.code=='auth/invalid-email'){
                this._showAlert('E-mail inválido!', 'O e-mail informado não está no formato correto, confira as informações.');
            }else{
                this._showAlert('Ocorreu um erro ao entrar na sua conta!', 'No momento estamos com problemas, por favor tente mais tarde.');
            }
        });
    }

    _onPressButtonLogin = () => {
        this.loginUser(this.state.email, this.state.password);
    }

    _onPressButtonWantSingup = () => {
        this.props.navigation.navigate('SingupScreen');
    }

    render(){
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <KeyboardAvoidingView style={styles.container} behavior='padding' enabled>
                        <ScrollView>
                            <View style={styles.containerForm}>

                                <Text style={ styles.formText }>Bem-vindo ao FanFilm!</Text>

                                <TextInput style={ styles.formField }
                                    placeholder='E-mail'
                                    value={this.state.email}
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={email => this.setState({ email })}
                                />

                                <TextInput style={ styles.formField }
                                    placeholder='Senha'
                                    value={this.state.password}
                                    secureTextEntry
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={password => this.setState({ password })}
                                />

                                <TouchableOpacity style={ styles.styleFormButton } onPress={this._onPressButtonLogin}>
                                    <Text style={styles.textFormButton}>Entrar</Text>
                                </TouchableOpacity>

                            </View>

                            <TouchableOpacity onPress={this._onPressButtonWantSingup}>
                                <Text style={ styles.styleTextButton }>Não tem uma conta? Cadastre-se agora.</Text>
                            </TouchableOpacity>

                        </ScrollView>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000'
    },
    formText: {
        margin: 10,
        fontSize: 25,
        fontWeight: 'bold',
        color: '#19aa7a'
    },
    styleFormButton:{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
    textFormButton: {
        color: '#fff',
        backgroundColor: '#19aa7a',
        padding: 15,
        paddingRight: 50,
        paddingLeft: 50,
        borderRadius: 10
    },
    containerForm: {
        flex: 1,
        width: 280
    },
    formField: {
        padding: 10,
        margin: 5,
        borderColor: '#eee',
        borderWidth: 1,
        borderRadius: 5,
        color: "#fff"
    },
    styleTextButton: {
        fontSize: 15,
        marginTop: 10,
        color: '#19aa7a',
        fontWeight: 'bold',
        padding: 15
    }
})

export default LoginScreen;