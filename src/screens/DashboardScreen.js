import React, { Component } from 'react';
import { SafeAreaView, ScrollView, View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import firebase from 'firebase';
import { ApiService } from '../api';

class DashboardScreen extends Component {
    constructor(props){
        super(props);

        this.state = {
            movies: []
        };
    }

    componentDidMount(){
        this.loadMoviesList();
        // setInterval(() =>  
        //     this.loadMoviesList(), 
        //     500
        // );
    }

    loadMoviesList = async () => {
        await ApiService.ApiMovie.getMoviesInADay().then(response => {
            response.json().then(movies => this.updateMoviesList(movies.results))
        });
    }

    updateMoviesList = async (movies) => {
        await this.setState({
            movies
        });
    }

    renderMovie = ({ item }) => {
        return(
            <View style={styles.containerContentMovies}>
                <Image 
                    style={styles.imagePoster}
                    resizeMode='contain'
                    source={{uri: `${ApiService.ApiMovie.getUrlImage()}${item.poster_path}`}} />
                <Text style={styles.textContentTitle}>{`${item.title}`}</Text>
                <Text style={styles.textContent}>{`Lançamento: ${item.release_date}`}</Text>
                <TouchableOpacity style={ styles.styleButtonDesire } onPress={this._onPressButtonLogin}>
                    <Text style={styles.textButtonDesire }>+ Lista de Desejos</Text>
                </TouchableOpacity>
                <TouchableOpacity style={ styles.styleButtonWatched } onPress={this._onPressButtonLogin}>
                    <Text style={styles.textButtonWatched }>+ Já Assistidos</Text>
                </TouchableOpacity>
            </View>
        );
    }

    render(){
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <ScrollView>
                        <View style={styles.containerContent}>
                            <FlatList 
                                contentContainerStyle={styles.textContent}
                                ListEmptyComponent={<Text style={styles.textContent}>Carregando...</Text>} 
                                data={this.state.movies}
                                keyExtractor={movie => `${movie.id}`}
                                renderItem={this.renderMovie}
                            />
                        </View> 
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000'
    },
    containerContent: {
        flex: 1
    },
    textContent: {
        flex: 1,
        marginTop: 15,
        marginBottom: 5,
        color: '#fff',
        fontSize: 16,
    },
    textContentTitle: {
        flex: 1,
        marginTop: 30,
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold'
    },
    containerContentMovies: {
        backgroundColor: '#19aa7a',
        margin: 5,
        padding: 10,
        borderRadius: 10
    },
    imagePoster: {
        alignSelf: 'center', 
        height: 500, 
        width: 500,  
        borderRadius: 10
    },
    styleButtonDesire:{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginTop: 10
    },
    styleButtonWatched:{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginTop: 10
    },
    textButtonDesire: {
        color: '#fff',
        backgroundColor: '#FF0000',
        padding: 15,
        paddingRight: 50,
        paddingLeft: 50,
        borderRadius: 10
    },
    textButtonWatched: {
        color: '#fff',
        backgroundColor: '#E8A90C',
        padding: 15,
        paddingRight: 50,
        paddingLeft: 50,
        borderRadius: 10
    }
})

export default DashboardScreen;