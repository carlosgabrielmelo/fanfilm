import firebase from 'firebase';

class Auth {
    login(email,password){
        return firebase.auth().signInWithEmailAndPassword(email,password);
    }

    singup(email,password){
        return firebase.auth().createUserWithEmailAndPassword(email,password);
    }
}

export default Auth;