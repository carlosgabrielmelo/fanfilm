import firebase from 'firebase';
import { firebaseConfig } from './config';

class ApiInit {
    Init(){
        firebase.initializeApp(firebaseConfig);
    }
}

export default ApiInit;