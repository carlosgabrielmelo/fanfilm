import Auth from './Auth';
import ApiInit from './ApiInit';
import ApiMovie from './ApiMovie';

class ApiService {
    static ApiInit = new ApiInit();
    static Auth = new Auth();
    static ApiMovie = new ApiMovie();
}

export { ApiService }