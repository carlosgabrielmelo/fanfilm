import { MOVIE_DB_API_KEY, BASE_URL_MOVIE, BASE_URL_IMAGE } from './Constants';

class ApiMovie {
    getMoviesInAWeek() {
        const URL_ENDPOINT = `${BASE_URL_MOVIE}trending/movie/week?api_key=${MOVIE_DB_API_KEY}`;

        return fetch(URL_ENDPOINT, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        });
    }

    getMoviesInADay() {
        const URL_ENDPOINT = `${BASE_URL_MOVIE}trending/movie/day?api_key=${MOVIE_DB_API_KEY}`;

        return fetch(URL_ENDPOINT, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        });
    }

    getUrlImage(){
        return BASE_URL_IMAGE;
    }
}

export default ApiMovie;